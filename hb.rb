if Gem.win_platform?
  Encoding.default_external = Encoding.find(Encoding.locale_charmap)
  Encoding.default_internal = __ENCODING__

  [STDIN, STDOUT].each do |io|
    io.set_encoding(Encoding.default_external, Encoding.default_internal)
  end
end
money_junior_developer = rand(1..3) * 100
money_developer = rand(3..8) * 100
money_senior_developer = rand(8..15) * 100

junior_developer = {
  name: 'Александр',
  age: '22',
  congratulations: 'Поздравляем с Днем Рождения, начинающий наш специалист!',
  gift: money_junior_developer
}
#
developer = {
  name: 'Виктор',
  age: '27',
  congratulations: 'Поздравляем с Днем Рождения! Вы успешно влились в наш коллектив и заняли в нем достойное место!',
  gift: money_developer
}

senior_developer = {
  name: 'Михаил',
  age: '32',
  congratulations: 'Поздравляем с Днем Рождения! В нашем коллективе Вы один из самых ценных сотрудников!',
  gift: money_senior_developer
}

def happy_birthday(person)
  puts "#{person[:name]}, сегодня Вам исполнилняется #{person[:age]}. #{person[:congratulations]}"
  puts "От всего коллектива хотим подарить Вам #{person[:gift]} долларов на интересное проведение своего дня рождения!"
end
happy_birthday(junior_developer)
puts
happy_birthday(developer)
puts
happy_birthday(senior_developer)

